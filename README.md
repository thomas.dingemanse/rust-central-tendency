# Central tendency of a vector

This is an implementation of one of the assignments from the [Rust book](https://doc.rust-lang.org/book/ch08-03-hash-maps.html#summary). It calculates the mean, median, and mode(s) for a given (sorted, mutable) vector. Currently it only works for i32 values, but it will be generalized to other types using generics.