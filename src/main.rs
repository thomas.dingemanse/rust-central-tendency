use std::collections::HashMap;

// TODO: use generics so it works for other types than i32.
// Note that not all data types have a mean (only ratio or interval data),
// and not all data types have a median (only ordinal, ratio or interval data).

fn main() {
    println!("\n[ Central tendency of a vector ]\n");

    // The data has to be sorted, but it doesn't have to be mutable.
    // Implementations where sorted (but not mutable) data is required are also possible,
    // as well as implementations that require neither (but have worse performance).
    let mut data = vec![45,8,15,17,84,512,12,0,0,4454,15,1,1,1,2];

    println!("Mean: {}", mean(&data));
    println!("Median: {}", median(&mut data));
    print!("Mode(s): ");

    // m: A vector containing 1 or more modes as i32.
    // n: The shared count of the modes as u32.
    let (m, n) = modes(&data);

    if m.len() > 1 {
        println!("{:?}, each occurring {} times in the data.", m, n);
    } else {
        println!("{}, occurring {} times in the data.", m[0], n);
    }
}

fn mean(data: &Vec<i32>) -> f32 {
    let mut sum = 0;

    for elem in data {
        sum += elem;
    }

    sum as f32 / data.len() as f32
}

fn median(data: &mut Vec<i32>) -> f32 {
    // A mutable reference to the data is required to sort the vector.
    // Another option would be to copy the vector first, but that would be slower.
    data.sort();

    if data.len() % 2 == 0 {
        // Take the average of the middle two if the vector has an even number of elements.
        (data[(data.len() / 2) - 1] + data[data.len() / 2]) as f32 / 2.0
    } else {
        // Otherwise the middle element is the median.
        data[data.len() / 2] as f32
    }
}

// Return a vector of 1 or more modes and their shared count as a tuple.
fn modes(data: &Vec<i32>) -> (Vec<i32>, u32) {
    // The hashmap stores the values with their associated count in the data.
    let mut map: HashMap<i32,u32> = HashMap::with_capacity(data.len());

    // There may be multiple modes, so they are stored in a vector.
    let mut modes: Vec<i32> = Vec::new();

    let mut max: u32 = 0;

    // Get counts for all unique elements in the data and store them in the hashmap.
    for elem in data {
        *map.entry(*elem).or_insert(0) += 1;
    }

    // Get the most frequently occuring element(s) from the hashmap.
    for (k,v) in map {
        if v > max {
            modes = vec![k];
            max = v;
        } else if v == max {
            modes.push(k);
        }
    }

    (modes, max)
}
